package com.aria.sdk.classes;


import static org.junit.Assert.*;

import org.junit.*;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;



import com.aria.sdk.classes.AriaBillingBuilder;
import com.aria.sdk.classes.AriaBillingComplete;
import com.aria.sdk.classes.BaseAriaBillingDTO;
import com.aria.sdk.classes.CallType;
import com.aria.sdk.classes.LibraryType;
import com.aria.sdk.classes.OutPutFormat;


public class FirstTest {
	

   

	
	@Test public void firstTest() throws Exception {
		
		long ERROR_CODE = 1004;
		Object ERROR_MESSAGE = "authentication error";
		
		
		/*REST CALL*/
	    BaseAriaBillingDTO baseAriaBillingDTO = new BaseAriaBillingDTO(
	                "https://secure.future.stage.ariasystems.net/api/ws/api_ws_class_dispatcher.php", "logger",
	                false/* Debug */, CallType.REST, OutPutFormat.OUTPUT_JSON, LibraryType.OBJECT_QUERY);


	    AriaBillingComplete ariaBillingComplete = AriaBillingBuilder.getAriaSDK(baseAriaBillingDTO);

	    AriaBillingIntegration ariaBillingIntegration = AriaBillingBuilder.getAriaObjectSDK(baseAriaBillingDTO);

	    
	    
	    com.aria.common.shared.EventListRow eventListRow1 = new com.aria.common.shared.EventListRow();
	    eventListRow1.setEventList(120L);
	    com.aria.common.shared.EventListRow eventListRow2 = new com.aria.common.shared.EventListRow();
	    eventListRow2.setEventList(130L);
	    com.aria.common.shared.EventListArray eventListArray = new com.aria.common.shared.EventListArray();
	    eventListArray.getEventListRow().add(eventListRow1);
	    eventListArray.getEventListRow().add(eventListRow2);

	    // Call the API
	    Map<String,Object> hashMapReturnValues = ariaBillingComplete.subscribeEvents(100L,"zzzz", eventListArray);
	
	    
	    
	    long aaa = (long) hashMapReturnValues.get("error_code");
	    
	    assertNotNull(aaa);
	    
	    assertEquals(aaa, ERROR_CODE);
	    assertEquals(hashMapReturnValues.get("error_msg"), ERROR_MESSAGE);

	}
}

